package Restaurant.src;
import java.util.ArrayList;
import java.util.Arrays;

public class Menu {
    private String title;
    private ArrayList<Item> items;

    public String getTitle() {
        return this.title;
    }

    public  ArrayList<Item> getItems() { 
        return this.items;
    }

    public Menu(String title) {
        this.title = title;
        this.items = new ArrayList<>();
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public String getName(Item item) {
        for(Item i: this.items) {
            if(Arrays.asList(i).contains(item)) break;
        }
        return item.getName();
    }

    public Boolean hasItem(Item item) {
        Boolean hasItem = false;
        for(Item i: this.items) {
            hasItem = Arrays.asList(i).contains(item);
            if(hasItem) break;
        }
        return hasItem;
    }
}
