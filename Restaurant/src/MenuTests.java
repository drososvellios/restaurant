package Restaurant.src;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


public class MenuTests {
    @Test
    public void a_Menu_has_title() {
        Restaurant Venice = new Restaurant("Venice");
        Menu menu = new Menu("Starters"); 
        assertEquals("Starters", menu.getTitle());
    }
    
    @Test
    public void a_Menu_items() {
        Restaurant Venice = new Restaurant("Venice");
        Menu menu = new Menu("Starters");
        Item soup = new Item("Onion Soup", 8.99);
        menu.addItem(soup);
        assertTrue(menu.hasItem(soup));
    }

}
