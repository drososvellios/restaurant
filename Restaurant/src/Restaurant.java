package Restaurant.src;

import java.util.ArrayList;

public class Restaurant {
    private String name; 
    private String imageUrl = "https://assets.bonappetit.com/photos/610aa6ddc50e2f9f7c42f7f8/master/pass/Savage-2019-top-50-busy-restaurant.jpg";
    private ArrayList<Menu> menus;

    public Restaurant(String name) {
        this.name = name;
        this.menus = new ArrayList<Menu>();
    }

    public String getName() {
        return this.name;
    }
    
    public String getImageUrl() {
        return this.imageUrl;
    }
    
    public ArrayList<Menu> getMenus() { 
        return this.menus;
    }

    public void addMenu(String title) {
        menus.add(new Menu(title));
    }

    public boolean isInMenuList() {
        for(Menu menu : this.menus)
            if(menu instanceof Menu)
                return true;
        return false;
    }
    
    public Boolean hasMenu(String title) { 
        boolean hasMenu = false; 
        if (isInMenuList()){
            hasMenu = true;
        }
        return hasMenu;
    }
    
}
