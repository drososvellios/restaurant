package Restaurant.src;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class RestaurantTests {

    @Test 
    public void a_Restaurant_has_name_and_image() {
        Restaurant Venice = new Restaurant("Venice");
        assertEquals(Venice.getName(), "Venice");
        assertEquals(Venice.getImageUrl(), "https://assets.bonappetit.com/photos/610aa6ddc50e2f9f7c42f7f8/master/pass/Savage-2019-top-50-busy-restaurant.jpg");
    }
 
    @Test
    public void a_Restaurant_has_menu() {  
        Restaurant Venice = new Restaurant("Venice");    
        Menu menu = new Menu("Starters");
        assertEquals(Venice.hasMenu("Starters"),false);
        Venice.addMenu("Starters");
        assertEquals(Venice.hasMenu("Starters"),true);
    }

}
