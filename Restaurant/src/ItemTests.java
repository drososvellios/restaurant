package Restaurant.src;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ItemTests {
    @Test
    public void a_item_has_a_name_and_a_price() {
        Restaurant Venice = new Restaurant("Venice");
        Menu menu = new Menu("Starters");
        Item soup = new Item("Onion Soup", 8.99);
        menu.addItem(soup);
        assertEquals("Onion Soup",soup.getName());
        assertEquals(8.99,soup.getPrice(),0);
    }
}
